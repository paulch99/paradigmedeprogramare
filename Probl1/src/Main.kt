import java.io.File
import java.lang.Exception
import java.sql.Timestamp
import java.text.SimpleDateFormat

fun main(args: Array<String>)
{
    //Citesc inregistrarile si impart textul linie cu linie
    val cale="D:\\AC\\syslog.txt"
    val lines:String=File(cale).readText()
    var s=lines.lines()
    val listaobj= mutableListOf<SyslogRecord>()
    val format=SimpleDateFormat("MM dd hh:mm:ss")

    val reg=Regex("Mar \\d\\d \\d\\d:\\d\\d:\\d\\d")


    s.forEach{
        if(reg.containsMatchIn(it))
        {
            var data=reg.find(it)!!.value
            data=data.replace("Mar","03")

            val pData=format.parse(data)
            val timestamp=Timestamp(pData.time)
            val subs=it.split(" ",":")
            val hostname=subs[6]
            val app_name=subs[7]
            var pid:Int=0
            var log_entry:String=""
            if(app_name.contains("[") && app_name.contains("]") ){
                pid=app_name.substring(app_name.indexOf("[")+1, app_name.indexOf("]")).toInt()
            }
            var i:Int=7

            while(i<subs.size)
            {
                log_entry=log_entry+subs[i]
                log_entry=log_entry+" "
                i=i+1
            }
            listaobj.add(SyslogRecord(timestamp, hostname, app_name, pid, log_entry))
        }
    }

    var objseq=listaobj.asSequence()
    var hm=HashMap<String,MutableList<SyslogRecord>>()

    //Parcurgerea secventei
    for(it in objseq)
    {
        if(hm.containsKey(it.app_name))
        {
            var tmp:MutableList<SyslogRecord>?
            tmp=hm.get(it.app_name)
            tmp?.add(it)
            hm.put(it.app_name, tmp!!)
        }
        else
        {
            var tmp:MutableList<SyslogRecord>?= mutableListOf()
            tmp?.add(it)
            hm.put(it.app_name, tmp!!)
        }
    }


    //sortarea descrierilor din log entry
    for(it in hm)
    {
        var tmp:MutableList<SyslogRecord>?=hm.get(it.key)
        tmp=tmp?.asSequence()?.sortedBy { it.log_entry }?.toMutableList()
        hm.put(it.key,tmp!!)
        println("(${it.key}, ${it.value})")
    }

    //print pentru inregistrarile care specifica un pid
    for(item in hm)
    {
    var tmp:MutableList<SyslogRecord>?=hm.get(item.key)
    println(tmp!!.filter{it.pid != 0})
    }
}