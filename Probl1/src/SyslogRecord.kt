import java.sql.*;

class SyslogRecord(val timestamp: Timestamp, val hostname:String, val app_name:String, var pid:Int=0, val log_entry:String){
    override fun toString(): String {
        return "${timestamp} ${hostname} ${app_name} ${pid} ${log_entry}"
    }
}